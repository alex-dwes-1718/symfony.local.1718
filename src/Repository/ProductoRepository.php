<?php

namespace App\Repository;

use App\Entity\Producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Producto::class);
    }

    public function findAllWithLimits($offset, $limit)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.categoria', 'cat')->addSelect('cat')
            ->orderBy('p.id', 'ASC')
            ->setMaxResults( $limit );

        if (!is_null($offset))
            $qb->setFirstResult( $offset );

        return $qb->getQuery()->getResult();
    }

    private function addBusqueda(QueryBuilder $qb, $busqueda=null)
    {
        if (!is_null($busqueda) && !empty($busqueda))
        {
            $qb ->where($qb->expr()->like('p.name', ':busqueda'))
                ->setParameter('busqueda', '%' . $busqueda . '%');
        }
    }

    public function findProductos($busqueda, $offset, $limit)
    {
        $qb = $this->createQueryBuilder('p');

        $qb ->join('p.categoria', 'cat')->addSelect('cat')
            ->orderBy('p.id', 'ASC')
            ->setMaxResults( $limit );

        $this->addBusqueda($qb, $busqueda);

        if (!is_null($offset))
            $qb->setFirstResult( $offset );

        return $qb->getQuery()->getResult();
    }

    public function getCountProductos($busqueda=null)
    {
        $qb = $this->createQueryBuilder('p');

        $qb ->select('count(p.id)');

        $this->addBusqueda($qb, $busqueda);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getProductosFiltrados($name, $precio, $categoria, $order)
    {
        $qb = $this->createQueryBuilder('producto');
        if (isset($name)) {
            $qb->where($qb->expr()->like('producto.name', ':name'))
                ->setParameter('name', '%' . $name . '%');
        }
        if (isset($precio)) {
            $qb->andWhere($qb->expr()->eq('producto.precio', ':precio'))
                ->setParameter('precio', $precio);
        }
        if (isset($categoria)) {
            $qb->innerJoin('producto.categoria', 'categoria');
            $qb->andWhere($qb->expr()->eq('categoria.nombre', ':categoria'))
                ->setParameter('categoria', $categoria);
        }

        $qb->orderBy('producto.' . $order, 'ASC');
        return $qb->getQuery()->getResult();
    }
}
