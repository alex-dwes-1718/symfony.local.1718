<?php

namespace App\BLL;


use App\Entity\Categoria;
use App\Entity\Producto;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductoBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function getProductos(string $busqueda = null, $offset=null, $limit=null)
    {
        return $this->em->getRepository(Producto::class)->findProductos($busqueda, $offset, $limit);
    }

    public function getNumProductos(string $busqueda=null)
    {
        return $this->em->getRepository(Producto::class)->getCountProductos($busqueda);
    }

    public function eliminaProducto($id)
    {
        $producto = $this->em->getRepository(Producto::class)->find($id);

        $this->em->remove($producto);
        $this->em->flush();
    }

    public function nuevo($name, $precio)
    {
        $producto = new Producto();
        $producto->setName($name);
        $producto->setPrecio($precio);
        $producto->setImagen('');

        $categoria = $this->em->getRepository(Categoria::class)
            ->findOneBy(['nombre' => 'deportes']);

        $producto->setCategoria($categoria);

        return $this->guardaValidando($producto);
    }

    public function guardaProducto(Producto $producto)
    {
        $this->em->persist($producto);
        $this->em->flush();
    }

    public function getProductosFiltrados($name, $precio, $categoria, $order)
    {
        $productos = $this->em->getRepository(Producto:: class )->getProductosFiltrados(
            $name, $precio, $categoria, $order);

        return $this->entitiesToArray($productos);
    }

    public function update(Producto $producto, array $data)
    {
        $categoria = $this->em->getRepository(Categoria::class)->find($data['categoria']);

        $producto->setName($data['name']);
        $producto->setPrecio($data['precio']);
        $producto->setCategoria($categoria);
        $producto->setDescripcion($data['descripcion']);

        return $this->guardaValidando($producto);
    }

    public function toArray($producto)
    {
        if ( is_null ($producto))
            return null;

        if (!($producto instanceof Producto))
            throw new \Exception("La entidad no es un Producto");

        return [
            'id' => $producto->getId(),
            'fechaAlta' => $producto->getFechaAlta()->format("d-m-Y H:i:s"),
            'name' => $producto->getName(),
            'precio' => $producto->getPrecio(),
            'imagen' => $producto->getImagen(),
            'categoria' => $producto->getCategoria()->getNombre()
//            'usuario' => $producto->getUsuario()->getUsername(),
        ];
    }

    public function cambiaImagen(Producto $producto, $imagenB64)
    {
        $fileName = $this->uploader->saveB64Imagen($imagenB64);

        $producto->setImagen($fileName);

        return $this->guardaValidando($producto);
    }
}