<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 12/02/18
 * Time: 16:43
 */

namespace App\BLL;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserBLL extends BaseBLL
{
    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function nuevo($username, $password, $email)
    {
        $user = new User();

        $user->setUsername($username);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $user->setRole('ROLE_ADMIN');

        return $this->guardaValidando($user);
    }

    public function getAll()
    {
        $users = $this->em->getRepository(User::class)->findAll();

        return $this->entitiesToArray($users);
    }

    public function toArray($user)
    {
        if ( is_null ($user))
            return null;

        if (!($user instanceof User))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'role' => $user->getRole()
        ];
    }

    public function profile()
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }

    public function cambiaPassword($nuevoPassword)
    {
        $user = $this->getUser();

        $user->setPassword($this->encoder->encodePassword($user, $nuevoPassword));

        return $this->guardaValidando($user);
    }

    public function getTokenByEmail($email)
    {
        $user = $this->em->getRepository(User:: class )
            ->findOneBy(array('email'=>$email));

        if ( is_null ($user))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($user);
    }
}