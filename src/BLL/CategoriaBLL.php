<?php

namespace App\BLL;


use App\Entity\Categoria;
use Doctrine\Common\Persistence\ObjectManager;

class CategoriaBLL extends BaseBLL
{
    private $categoriaRepository;

    public function getCategorias(string $busqueda=null)
    {
        if (is_null($busqueda))
            return $this->categoriaRepository->findAll();
        else
            return $this->categoriaRepository->findByNombre($busqueda);
    }

    public function eliminaCategoria($id)
    {
        $categoria = $this->categoriaRepository->find($id);

        $this->em->remove($categoria);
        $this->em->flush();
    }

    public function guardaCategoria(Categoria $categoria)
    {
        $this->em->persist($categoria);
        $this->em->flush();
    }

    public function toArray($entity)
    {
        // TODO: Implement toArray() method.
    }
}