<?php

namespace App\Controller;

use App\BLL\CategoriaBLL;
use App\Entity\Categoria;
use App\Form\CategoriaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/categorias")
 */
class CategoriaController extends Controller
{
    /**
     * @Route("/", name="sanvipop_categorias")
     * @Template("categorias/categorias.html.twig")
     */
    public function index(CategoriaBLL $categoriaBLL)
    {
        $categorias = $categoriaBLL->getCategorias();

        return ['categorias' => $categorias];
    }

    private function formCategoria(Request $request, CategoriaBLL $categoriaBLL, Categoria $categoria)
    {
        $form = $this->createForm(CategoriaType::class, $categoria);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $categoriaBLL->guardaCategoria($categoria);

            return $this->redirectToRoute('sanvipop_categorias');
        }

        return $this->render(
            'categorias/form-categoria.html.twig',
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/nuevo", name="sanvipop_categorias_nuevo")
     * @Method({"GET", "POST"})
     */
    public function nuevo(Request $request, CategoriaBLL $CategoriaBLL)
    {
        $Categoria = new Categoria();

        return $this->formCategoria($request, $CategoriaBLL, $Categoria);
    }

    /**
     * @Route("/{id}/editar", name="sanvipop_categorias_editar")
     * @Method({"GET", "POST"})
     */
    public function editar(Request $request, CategoriaBLL $CategoriaBLL, Categoria $Categoria)
    {
        return $this->formCategoria($request, $CategoriaBLL, $Categoria);
    }

    /**
     * @Route("/eliminar/{id}", name="sanvipop_categorias_eliminar")
     * @Method("GET")
     */
    public function eliminar(CategoriaBLL $CategoriaBLL, $id)
    {
        $CategoriaBLL->eliminaCategoria($id);

        return $this->redirectToRoute('sanvipop_categorias');
    }

    /**
     * @Route("/buscar", name="sanvipop_categorias_buscar")
     * @Method("POST")
     * @Template("categorias/categorias.html.twig")
     */
    public function buscar(Request $request, CategoriaBLL $categoriaBLL)
    {
        $busqueda = $request->request->get('busqueda');

        $categorias = $categoriaBLL->getCategorias($busqueda);

        return ['categorias' => $categorias];
    }
}
