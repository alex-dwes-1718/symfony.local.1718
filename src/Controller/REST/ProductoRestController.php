<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 12/02/18
 * Time: 16:48
 */

namespace App\Controller\REST;

use App\BLL\ProductoBLL;
use App\Entity\Producto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductoRestController extends BaseApiController
{
    /**
     * @Route("/productos.{_format}", name="get_productos",
     * defaults={"_format": "json"},
     * requirements={"_format": "json"}
     * )
     * @Route("/productos/ordenados/{order}", name="get_productos_ordenados",
     *  requirements={"order": "name|precio|categoria"}
     * )
     * @Method("GET")
     */
    public function getAll(Request $request, ProductoBLL $productoBLL, $order='name')
    {
        $name = $request->query->get('name');
        $precio = $request->query->get('precio');
        $categoria = $request->query->get('categoria');

        $productos = $productoBLL->getProductosFiltrados($name, $precio, $categoria, $order);

        return $this->getResponse($productos);
    }

    /**
     * @Route("/productos/{id}.{_format}", name="get_producto",
     * requirements={"id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("GET")
     */
    public function getOne(Producto $producto, ProductoBLL $productoBLL)
    {
        return $this->getResponse($productoBLL->toArray($producto));
    }

    /**
     * @Route("/productos.{_format}", name="post_productos",
     *  defaults={"_format": "json"},
     *  requirements={"_format": "json"}
     * )
     * @Method("POST")
     */
    public function post(Request $request, ProductoBLL $productoBLL)
    {
        $data = $this->getContent($request);

        $producto = $productoBLL->nuevo(
            $data['name'],
            $data['precio']);

        return $this->getResponse($producto, Response::HTTP_CREATED );
    }

    /**
     * @Route("/productos/{id}.{_format}", name="update_producto",
     *  requirements={"id": "\d+", "_format": "json" },
     *  defaults={"_format": "json"})
     * @Method("PUT")
     */
    public function update(Request $request, Producto $producto, ProductoBLL $productoBLL)
    {
        $data = $this->getContent($request);
        $producto = $productoBLL->update($producto, $data);
        return $this->getResponse($producto, Response:: HTTP_OK );
    }

    /**
     * @Route("/productos/{id}.{_format}", name="delete_producto",
     * requirements={ "id": "\d+", "_format": "json" },
     * defaults={"_format": "json"})
     * @Method("DELETE")
     */
    public function delete(Producto $producto, ProductoBLL $productoBLL)
    {
        $productoBLL->delete($producto);
        return $this->getResponse(null, Response:: HTTP_NO_CONTENT );
    }

    /**
     * @Route("/productos/{id}/imagen.{_format}", name="cambia_imagen_producto",
     * requirements={"id": "\d+", "_format": "json"},
     * defaults={"_format": "json"})
     * @Method("PATCH")
     */
    public function cambiaImagen(
        Request $request, ProductoBLL $productoBLL, Producto $producto)
    {
        $data = $this->getContent($request);
        if ( is_null ($data['imagen']))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $producto = $productoBLL->cambiaImagen(
            $producto, $data['imagen']);

        return $this->getResponse($producto);
    }
}