<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 14/02/18
 * Time: 17:47
 */

namespace App\Controller\REST;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthRestController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        // The security layer will intercept this request
        return new Response('', Response:: HTTP_UNAUTHORIZED );
    }
}