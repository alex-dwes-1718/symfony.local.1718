<?php

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

class PagesController
{
    /**
     * @Route("/", name="sanvipop_main")
     * @Template("main.html.twig")
     */
    public function main()
    {
        return [];
    }
}