<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 15/01/18
 * Time: 17:46
 */

namespace App\Controller;

use App\BLL\ProductoBLL;
use App\Entity\Producto;
use App\Form\ProductoType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/productos")
 */
class ProductoController extends Controller
{
    private function getProductos(
        ProductoBLL $productoBLL,
        string $busqueda=null,
        string $page=null)
    {
        $numProductosPagina = 3;
        if (is_null($page))
        {
            $offset = null;
            $page = 1;
        }
        else
            $offset = ($page-1) * $numProductosPagina;

        $productos = $productoBLL->getProductos($busqueda, $offset, $numProductosPagina);

        if (!is_null($busqueda))
        {
            $numProductosFiltrados = $productoBLL->getNumProductos($busqueda);
            $numProductos = $productoBLL->getNumProductos();
        }
        else
        {
            $numProductos = $productoBLL->getNumProductos();
            $numProductosFiltrados = $numProductos;
        }

        $user = $this->getUser();

        return [
            'productos' => $productos,
            'numProductos' => $numProductos,
            'numProductosPagina' => $numProductosPagina,
            'numProductosFiltrados' => $numProductosFiltrados,
            'busqueda' => $busqueda,
            'page' => $page,
            'usuario' => $user];
    }
    /**
     * @Route("/", name="sanvipop_productos")
     * @Template("productos/productos.html.twig")
     */
    public function listar(Request $request, ProductoBLL $productoBLL)
    {
        $page = $request->query->get('page');
        $busqueda = $request->query->get('busqueda');

        return $this->getProductos($productoBLL, $busqueda, $page);
    }

    private function formProducto(
        Request $request,
        ProductoBLL $productoBLL,
        Producto $producto)
    {
        $form = $this->createForm(ProductoType::class, $producto);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $productoBLL->guardaProducto($producto);

            return $this->redirectToRoute('sanvipop_productos');
        }

        return $this->render(
            'productos/form-producto.html.twig',
            [
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @Route("/nuevo", name="sanvipop_productos_nuevo")
     * @Method({"GET", "POST"})
    */
    public function nuevo(Request $request, ProductoBLL $productoBLL)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $producto = new Producto();

        return $this->formProducto($request, $productoBLL, $producto);
    }

    /**
     * @Route("/{id}/editar", name="sanvipop_productos_editar")
     * @Method({"GET", "POST"})
     */
    public function editar(
        Request $request, ProductoBLL $productoBLL, Producto $producto)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $producto->setImagen(
            new File(
                $this->getParameter('products_directory') . '/' . $producto->getImagen()
            )
        );
        return $this->formProducto($request, $productoBLL, $producto);
    }

    /**
     * @Route("/{id}/eliminar", name="sanvipop_productos_eliminar")
     * @Method("GET")
     */
    public function eliminar(ProductoBLL $productoBLL, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $productoBLL->eliminaProducto($id);

        return $this->redirectToRoute('sanvipop_productos');
    }

    /**
     * @Route("/buscar", name="sanvipop_productos_buscar")
     * @Method("POST")
     * @Template("productos/productos.html.twig")
     */
    public function buscar(Request $request, ProductoBLL $productoBLL)
    {
        $busqueda = $request->request->get('busqueda');

        return $this->getProductos($productoBLL, $busqueda);
    }
}