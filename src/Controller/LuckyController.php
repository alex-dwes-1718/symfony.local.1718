<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 9/01/18
 * Time: 18:01
 */

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lucky")
 */
class LuckyController extends Controller
{
    /**
     * @Route("/")
     */
    public function number()
    {
        $number = mt_rand(0, 100);

        return $this->render(
            'lucky/number.html.twig',
            array(
                'number' => $number
            ));
    }

    /**
     * @Route(
     * "/articles/{_locale}/{year}/{slug}.{_format}",
     * defaults={"_format": "html"},
     * requirements={
     * "_locale": "en|fr",
     * "_format": "html|rss|json",
     * "year": "\d+"
     * }
     * )
     * @Template("articles.html.twig")
     */
    public function show($_locale, $year, $slug, $_format)
    {
        $articles = [
            [
                'nombre' => 'pelota',
                'precio' => 10
            ],
            [
                'nombre' => 'calceta',
                'precio' => 5
            ]
        ];

        if ($_format === 'html')
            return [ 'articles' => $articles];

        return new JsonResponse($articles);
    }
}